package com.eficacia.service;

import java.util.List;

import com.eficacia.model.User;

public interface UserService {
	
	User createUser (User user);
	
	User updateUser (User user);
	
	User findUserById(int id);
	
	User findUserByEmail(String email);
	
	List<User> findAllUsers();
}
