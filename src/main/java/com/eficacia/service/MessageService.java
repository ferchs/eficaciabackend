package com.eficacia.service;

import com.eficacia.model.Message;

public interface MessageService {
	
	Message createMessage (Message message);
	
	Message findMessageById(int id);
}
