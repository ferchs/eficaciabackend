package com.eficacia.service;

import java.util.List;

import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.eficacia.dao.GenericDao;
import com.eficacia.model.User;

@Service("userService")
@Transactional
public class UserServiceImpl implements UserService {

	GenericDao<User, Integer> userDao;
	
	@Autowired
	public void setDao( GenericDao< User,Integer> daoToSet ){
		userDao = daoToSet;
		userDao.setEntityClass( User.class );
	}
	
	@Override
	public User createUser(User user) {
		// TODO Auto-generated method stub
		return userDao.create(user);
	}

	@Override
	public User findUserById(int id) {
		// TODO Auto-generated method stub
		return userDao.findById(id);
	}

	@Override
	public User findUserByEmail(String email) {
		// TODO Auto-generated method stub
		return userDao.findByField("email", email);
	}

	@Override
	public List<User> findAllUsers() {
		// TODO Auto-generated method stub
		return userDao.findAll();
	}

	@Override
	public User updateUser(User user) {
		// TODO Auto-generated method stub
		return userDao.update(user);
	}
	

}
