package com.eficacia.security;

import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;
import com.eficacia.service.UserService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
public class TokenService {
		
	@Value("${app.jwt-secret-key}")
	public String SECRET;
	public static final long EXPIRATION_TIME = 86_400_000;
	public static final String TOKEN_PREFIX = "Bearer ";
	public static final String HEADER_STRING = "Authorization";
	
	@Autowired
	private UserService userService;
	
	
	public TokenService() {
		
	}
	
	public boolean isTokenExpired(String token) {
		Claims claims = Jwts.parser().setSigningKey(SECRET).parseClaimsJws(token).getBody();
		Integer intDate = (Integer) claims.get("exp");
		Date tokenDate = new Date(intDate.longValue() * 1000);
		Date currentDate = new Date();
		if (currentDate.before(tokenDate)) {
			return true;
		} else {
			return false;
		}
	}
	
	public String getSubjectFromEmailToken(String token) {
		Claims claims = Jwts.parser().setSigningKey(SECRET).parseClaimsJws(token).getBody();
		return (String) claims.get("sub");
	}
	
	public String createAuthenticationToken(Authentication auth) {
		String token = Jwts.builder()
				.setSubject(((User) auth.getPrincipal()).getUsername())
				.setIssuedAt(new Date())
				.setExpiration(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
				.signWith(SignatureAlgorithm.HS512, SECRET.getBytes()).compact();
		return token;
	}
	
	public long getExpirationTime(String token) {
		Claims claims = Jwts.parser().setSigningKey(SECRET.getBytes()).parseClaimsJws(token).getBody();
		Integer intDate = (Integer) claims.get("exp");
		long tokenDate = (intDate.longValue() * 1000);
		return tokenDate;
	}
	
	public String getSubjectFromAuthorizationToken(String token) {
		String subject = Jwts.parser().setSigningKey(SECRET.getBytes()).parseClaimsJws(token.replace(TOKEN_PREFIX, ""))
				.getBody().getSubject();
		return subject;
	}
	
	public boolean userExist(String email) {
		System.out.println(userService.findUserByEmail(email).getId());
		return userService.findUserByEmail(email)!=null;
	}
	

}