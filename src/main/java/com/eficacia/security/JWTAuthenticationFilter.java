package com.eficacia.security;

import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.context.ApplicationContext;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import com.eficacia.dto.LoginDto;
import com.eficacia.model.User;
import com.eficacia.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;


public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
	
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
   
    private AuthenticationManager authenticationManager;
    
    private TokenService tokenService;
    
    private UserService userService;
        
    private int userId;
    
    
    public JWTAuthenticationFilter(AuthenticationManager authenticationManager, ApplicationContext ctx) {
        this.authenticationManager = authenticationManager;
        tokenService=ctx.getBean(TokenService.class);
        userService=ctx.getBean(UserService.class);
    }

    @Override
	public Authentication attemptAuthentication(HttpServletRequest req, HttpServletResponse res)
			throws AuthenticationException {
		
		try {
			LoginDto creds = new ObjectMapper().readValue(req.getInputStream(), LoginDto.class);
			User user = userService.findUserByEmail(creds.getEmail());
			if (user == null) {
				return authenticationManager.authenticate(new UsernamePasswordAuthenticationToken("","", new ArrayList<>()));
			}else {
				userId=user.getId();
				return authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(creds.getEmail(),
						creds.getPassword(), new ArrayList<>()));
			}
			
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	protected void successfulAuthentication(HttpServletRequest req, HttpServletResponse res, FilterChain chain,
			Authentication auth) throws IOException, ServletException {
		System.out.println("Logueado Perfectamente....");
		String token = tokenService.createAuthenticationToken(auth);
		long expirationTime = tokenService.getExpirationTime(token);
		res.addHeader("Access-Control-Expose-Headers", "Authorization");
		res.addHeader(HEADER_STRING, TOKEN_PREFIX + token);
		res.setHeader("Content-Type", "application/json");
		res.getWriter().print("{\"expiresIn\": " + expirationTime + ",\"userId\": "+ this.userId+ "}");
	}
	
	@Override
	protected void unsuccessfulAuthentication(HttpServletRequest request,
			HttpServletResponse response, AuthenticationException failed)
			throws IOException, ServletException {
		SecurityContextHolder.clearContext();
		
		if(failed.getClass().equals(UsernameNotFoundException.class)) {
			response.sendError(404,failed.getMessage());	
		}
	}
	    
}