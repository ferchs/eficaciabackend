package com.eficacia.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserMessageDto {
	private String message;
	private String labeledId;
}
