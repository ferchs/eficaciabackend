package com.eficacia.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserGetDto {
	
	private int id;
	
	private String username;
	
	private String name;
	
	private String lastname;
	
	private String email;
}
