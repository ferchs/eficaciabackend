package com.eficacia.controller;

import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.util.UriComponentsBuilder;
import com.eficacia.dto.UserMessageDto;
import com.eficacia.model.Message;
import com.eficacia.model.User;
import com.eficacia.service.UserService;

@Controller
@RequestMapping("/v1")
public class TimelineController {
	
	@Autowired
	private UserService userService;

	@RequestMapping(value = "/users/{id}/messages", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	@Transactional
	public ResponseEntity<Set<?>> getUserMessages(@PathVariable("id") String userId,UriComponentsBuilder uriComponentsBuilder, final HttpServletRequest request) {
		Set<Message> messages =userService.findUserById(Integer.parseInt(userId)).getMessages();
		if (messages.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<Set<?>>(messages, HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/users/{id}/messages", method = RequestMethod.POST, headers = "Accept=application/json")
	@ResponseBody
	@Transactional
	public ResponseEntity<String> registerMessage(@PathVariable("id") String userId, @RequestBody UserMessageDto userMessage,
			UriComponentsBuilder uriComponentsBuilder, final HttpServletRequest request) {
		User user=userService.findUserById(Integer.parseInt(userId));
		if (user == null) {
			return new ResponseEntity<>("User with id " + userId + " not found",
					HttpStatus.NOT_FOUND);
		}else {
			if(userMessage.getLabeledId()==null) {
				Message message= new Message();
				message.setContent(userMessage.getMessage());
				user.addMessage(message);
				userService.updateUser(user);
				return new ResponseEntity<String>(HttpStatus.CREATED);
			}else {
				User labeledUser=userService.findUserById(Integer.parseInt(userId));
				Message message= new Message();
				message.setContent(userMessage.getMessage());
				user.addMessage(message);
				userService.updateUser(user);
				labeledUser.addMessage(message);
				userService.updateUser(labeledUser);
				return new ResponseEntity<String>(HttpStatus.CREATED);
			}
		}
		
	}
	
}
