package com.eficacia.controller;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.util.UriComponentsBuilder;
import com.eficacia.annotation.DTO;
import com.eficacia.dto.UserDto;
import com.eficacia.dto.UserGetDto;
import com.eficacia.model.User;
import com.eficacia.service.UserService;

@Controller
@RequestMapping("/v1")
public class UserController {

	@Autowired
	private UserService userService;
	
	@Autowired
	private ModelMapper modelMapper;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@RequestMapping(value = "/users", method = RequestMethod.POST, headers = "Accept=application/json")
	@ResponseBody
	@Transactional
	public ResponseEntity<String> registerUser(@Valid @DTO(UserDto.class) User user,
			UriComponentsBuilder uriComponentsBuilder, final HttpServletRequest request) {
		user.setPassword(passwordEncoder.encode(user.getPassword()));
		userService.createUser(user);
		return new ResponseEntity<String>(HttpStatus.CREATED);
	}
	
	@RequestMapping(value = "/users", method = RequestMethod.GET, headers = "Accept=application/json")
	@Transactional
	public ResponseEntity<List<?>> getUsers(UriComponentsBuilder uriComponentsBuilder) {
		List<User> users = new ArrayList<User>();
		users = userService.findAllUsers();
		if (users.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<List<?>>(users, HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/users/{id}", method = RequestMethod.GET)
	@Transactional
	public ResponseEntity<?> getUserById(@PathVariable("id") String userId, Principal principal,
			UriComponentsBuilder uriComponentsBuilder) {
		User user = userService.findUserById(Integer.parseInt(userId));
		if (user == null) {
			return new ResponseEntity<>("User with id " + userId + " not found",
					HttpStatus.NOT_FOUND);
		}
		
		UserGetDto dto=modelMapper.map(user, UserGetDto.class);
		
		return new ResponseEntity<UserGetDto>(dto, HttpStatus.OK);
	}
}
