package com.eficacia;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EfiTwitterApplication {

	public static void main(String[] args) {
		SpringApplication.run(EfiTwitterApplication.class, args);
	}
}
